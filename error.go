package simply

import (
	"errors"
	"fmt"
	"net/http"
)

var (
	ErrNotFound              = NewError(http.StatusNotFound, http.StatusText(http.StatusNotFound))
	ErrNotImplemented        = NewError(http.StatusNotImplemented, http.StatusText(http.StatusNotImplemented))
	ErrRendererNotRegistered = errors.New("renderer not registered")
)

func NewError(code int, message any) *Error {
	return &Error{Code: code, Message: message}
}

type Error struct {
	Code    int         `json:"code"`
	Message interface{} `json:"message"`
}

func (e *Error) Error() string {
	return fmt.Sprintf("[%d] %s", e.Code, e.Message)
}

type ErrorHandler func(ctx Context, err error)

func errorHandler() ErrorHandler {
	return func(ctx Context, err error) {
		var response = &Error{
			Code:    http.StatusInternalServerError,
			Message: http.StatusText(http.StatusInternalServerError),
		}

		switch e := err.(type) {
		case *Error:
			response = e
		default:
			response.Message = e.Error()
		}

		if retErr := ctx.Error(response.Code, response); retErr != nil {
			ctx.Logger().Error(response)
		}
	}
}
