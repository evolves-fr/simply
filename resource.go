package simply

type Resource interface {
	List(c Context) error
	Show(c Context) error
	Create(c Context) error
	Update(c Context) error
	Delete(c Context) error
}

type AppResource interface {
	Resource
	New(c Context) error
	Edit(c Context) error
	Remove(c Context) error
}

type ResourceName interface {
	Name() string
}

type ResourceKey interface {
	Key() string
}

type DefaultResource struct{}

func (DefaultResource) List(Context) error {
	return ErrNotImplemented
}

func (DefaultResource) Show(Context) error {
	return ErrNotImplemented
}

func (DefaultResource) Create(Context) error {
	return ErrNotImplemented
}

func (DefaultResource) Update(Context) error {
	return ErrNotImplemented
}

func (DefaultResource) Delete(Context) error {
	return ErrNotImplemented
}

type DefaultAppResource struct {
	DefaultResource
}

func (DefaultAppResource) New(Context) error {
	return ErrNotImplemented
}

func (DefaultAppResource) Edit(Context) error {
	return ErrNotImplemented
}

func (DefaultAppResource) Remove(Context) error {
	return ErrNotImplemented
}
