package simply

type Route struct {
	Name    string
	Method  string
	Path    string
	Handler HandlerFunc
}
