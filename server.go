package simply

import (
	"io/fs"
	"io/ioutil"
	"mime"
	"net/http"
	"os"
	"path/filepath"
	"reflect"
	"runtime"
	"strings"

	"github.com/gobuffalo/flect"
	"github.com/gorilla/mux"
	"github.com/olekukonko/tablewriter"
)

type Server interface {
	SetContext(fn func(c Context) Context)
	SetLogger(l Logger)
	SetRenderer(r Renderer)
	SetPublic(fs fs.FS)
	Store() Store
	Register(r *Route)
	Add(method, path string, h HandlerFunc) *Route
	Get(path string, h HandlerFunc) *Route
	Head(path string, h HandlerFunc) *Route
	Post(path string, h HandlerFunc) *Route
	Put(path string, h HandlerFunc) *Route
	Patch(path string, h HandlerFunc) *Route
	Delete(path string, h HandlerFunc) *Route
	Connect(path string, h HandlerFunc) *Route
	Options(path string, h HandlerFunc) *Route
	Trace(path string, h HandlerFunc) *Route
	Ressource(path string, r Resource)
	Handler() http.Handler
	Start(address string) error
}

func New() (Server, error) {
	return &server{
		router:       mux.NewRouter(),
		logger:       NewLogger(LevelTrace, os.Stdout),
		store:        NewStore(),
		renderer:     nil,
		context:      nil,
		errorHandler: errorHandler(),
		routes:       make([]*Route, 0),
	}, nil
}

type server struct {
	router       *mux.Router
	logger       Logger
	store        Store
	renderer     Renderer
	context      func(c Context) Context
	errorHandler ErrorHandler
	public       fs.FS
	routes       []*Route
}

func (s *server) SetContext(fn func(c Context) Context) {
	s.context = fn
}

func (s *server) SetLogger(l Logger) {
	s.logger = l
}

func (s *server) SetRenderer(r Renderer) {
	s.renderer = r
}

func (s *server) SetPublic(fs fs.FS) {
	s.public = fs
}

func (s *server) Store() Store {
	return s.store
}

func (s *server) Register(g *Route) {
	if g.Name == "" {
		g.Name = runtime.FuncForPC(reflect.ValueOf(g.Handler).Pointer()).Name()
	}

	s.routes = append(s.routes, g)
}

func (s *server) Add(method, path string, h HandlerFunc) *Route {
	var r = &Route{Method: method, Path: path, Handler: h}
	s.Register(r)
	return r
}

func (s *server) Get(path string, h HandlerFunc) *Route {
	return s.Add(http.MethodGet, path, h)
}

func (s *server) Head(path string, h HandlerFunc) *Route {
	return s.Add(http.MethodHead, path, h)
}

func (s *server) Post(path string, h HandlerFunc) *Route {
	return s.Add(http.MethodPost, path, h)
}

func (s *server) Put(path string, h HandlerFunc) *Route {
	return s.Add(http.MethodPut, path, h)
}

func (s *server) Patch(path string, h HandlerFunc) *Route {
	return s.Add(http.MethodPatch, path, h)
}

func (s *server) Delete(path string, h HandlerFunc) *Route {
	return s.Add(http.MethodDelete, path, h)
}

func (s *server) Connect(path string, h HandlerFunc) *Route {
	return s.Add(http.MethodConnect, path, h)
}

func (s *server) Options(path string, h HandlerFunc) *Route {
	return s.Add(http.MethodOptions, path, h)
}

func (s *server) Trace(path string, h HandlerFunc) *Route {
	return s.Add(http.MethodTrace, path, h)
}

func (s *server) Ressource(path string, r Resource) {
	// Get resource name
	var resourceName = reflect.TypeOf(r).Name()
	if resourceNameFunc, ok := r.(ResourceName); ok {
		resourceName = resourceNameFunc.Name()
	}
	var resourceNameSingularized = flect.Singularize(resourceName)

	// Get resource key
	var resourceKey = "id"
	if resourceKeyFunc, ok := r.(ResourceKey); ok {
		resourceKey = resourceKeyFunc.Key()
	}

	// Set paths
	var (
		shortPath = path
		fullPath  = filepath.Join(path, "{"+resourceKey+"}")
	)

	if shortPath != "/" && strings.HasSuffix(shortPath, "/") {
		shortPath = strings.TrimSuffix(path, "/")
	}

	// Register resource route
	s.Register(&Route{
		Name:    "List" + resourceName,
		Method:  http.MethodGet,
		Path:    shortPath,
		Handler: r.List,
	})
	s.Register(&Route{
		Name:    "Create" + resourceNameSingularized,
		Method:  http.MethodPost,
		Path:    shortPath,
		Handler: r.Create,
	})
	s.Register(&Route{
		Name:    "Show" + resourceNameSingularized,
		Method:  http.MethodGet,
		Path:    fullPath,
		Handler: r.Show,
	})
	s.Register(&Route{
		Name:    "Update" + resourceNameSingularized,
		Method:  http.MethodPut,
		Path:    fullPath,
		Handler: r.Update,
	})
	s.Register(&Route{
		Name:    "Delete" + resourceNameSingularized,
		Method:  http.MethodDelete,
		Path:    fullPath,
		Handler: r.Delete,
	})

	// Register app resource routes
	if ar, ok := r.(AppResource); ok {
		s.Register(&Route{
			Name:    "New" + resourceNameSingularized,
			Method:  http.MethodGet,
			Path:    filepath.Join(shortPath, "new"),
			Handler: ar.New,
		})
		s.Register(&Route{
			Name:    "Edit" + resourceNameSingularized,
			Method:  http.MethodGet,
			Path:    filepath.Join(shortPath, "edit"),
			Handler: ar.Edit,
		})
		s.Register(&Route{
			Name:    "Remove" + resourceNameSingularized,
			Method:  http.MethodGet,
			Path:    filepath.Join(shortPath, "remove"),
			Handler: ar.Remove,
		})
	}

}

func (s *server) Handler() http.Handler {
	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{"Name", "Method", "Path"})

	for _, route := range s.routes {
		var r = s.router.HandleFunc(route.Path, WrapHandler(s, route.Handler))
		r.Name(route.Name)
		r.Methods(route.Method)

		table.Append([]string{route.Name, route.Method, route.Path})
	}

	if s.public != nil {
		s.router.PathPrefix("/").HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			var fn = func(c Context) error {
				file, err := s.public.Open(filepath.Join("public/", c.Request().RequestURI))
				if err != nil {
					return ErrNotFound
				}

				content, err := ioutil.ReadAll(file)
				if err != nil {
					return ErrNotFound
				}

				return c.Blob(http.StatusOK, mime.TypeByExtension(filepath.Ext(c.Request().URL.Path)), content)
			}

			var c = newContext(s, w, r)
			if s.context != nil {
				c = s.context(c)
			}

			if err := fn(c); err != nil {
				s.errorHandler(c, err)
			}
		})
		table.Append([]string{"Public", "GET", "/*"})
	}

	table.Render()

	return s.router
}

func (s *server) Start(address string) error {
	return http.ListenAndServe(address, s.Handler())
}
