package simply

import (
	"bytes"
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
)

type Context interface {
	Request() *http.Request
	Response() http.ResponseWriter
	Logger() Logger
	Store() Store
	Param(key string) string
	Error(code int, err error) error

	HTML(code int, html string) error
	HTMLBlob(code int, b []byte) error
	JSON(code int, i any) error
	JSONPretty(code int, i any, indent string) error
	Blob(code int, contentType string, b []byte) error
	Render(code int, view string, data any) error
	RenderWithLayout(code int, layout, view string, data any) error
}

func newContext(srv *server, w http.ResponseWriter, r *http.Request) Context {
	return &context{
		request:  r,
		response: w,
		logger:   srv.logger,
		renderer: srv.renderer,
		store:    srv.store,
		params:   mux.Vars(r),
	}
}

type context struct {
	request  *http.Request
	response http.ResponseWriter
	logger   Logger
	renderer Renderer
	store    Store
	params   map[string]string
}

func (c context) Request() *http.Request {
	return c.request
}

func (c context) Response() http.ResponseWriter {
	return c.response
}

func (c *context) Logger() Logger {
	return c.logger
}

func (c *context) Store() Store {
	return c.store
}

func (c *context) Param(key string) string {
	return c.params[key]
}

func (c *context) Error(code int, e error) error {
	buf := new(bytes.Buffer)

	if err := c.renderer.Error(buf, &Error{Code: code, Message: e}, c); err != nil {
		return c.JSON(code, e)
	}

	return c.HTMLBlob(code, buf.Bytes())
}

func (c *context) HTML(code int, html string) error {
	return c.HTMLBlob(code, []byte(html))
}

func (c *context) HTMLBlob(code int, b []byte) error {
	return c.Blob(code, MIMETextHTMLCharsetUTF8, b)
}

func (c context) JSON(code int, i any) error {
	res, err := json.Marshal(i)
	if err != nil {
		return err
	}

	return c.Blob(code, MIMEApplicationJSONCharsetUTF8, res)
}

func (c context) JSONPretty(code int, i any, indent string) error {
	res, err := json.MarshalIndent(i, "", indent)
	if err != nil {
		return err
	}

	return c.Blob(code, MIMEApplicationJSONCharsetUTF8, res)
}

func (c *context) Blob(code int, contentType string, b []byte) error {
	header := c.response.Header()
	if header.Get(HeaderContentType) == "" {
		header.Set(HeaderContentType, contentType)
	}
	c.response.WriteHeader(code)
	_, err := c.response.Write(b)
	return err
}

func (c *context) Render(code int, view string, data any) error {
	if c.renderer == nil {
		return ErrRendererNotRegistered
	}

	buf := new(bytes.Buffer)
	if err := c.renderer.Render(buf, view, data, c); err != nil {
		return err
	}
	return c.HTMLBlob(code, buf.Bytes())
}

func (c *context) RenderWithLayout(code int, layout, view string, data any) error {
	if c.renderer == nil {
		return ErrRendererNotRegistered
	}

	buf := new(bytes.Buffer)
	if err := c.renderer.RenderWithLayout(buf, layout, view, data, c); err != nil {
		return err
	}
	return c.HTMLBlob(code, buf.Bytes())
}
