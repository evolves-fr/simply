package simply

import "net/http"

// HandlerFunc defines a function to serve HTTP requests.
type HandlerFunc func(c Context) error

func WrapHandler(s *server, h HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var c = newContext(s, w, r)
		if s.context != nil {
			c = s.context(c)
		}

		if err := h(c); err != nil {
			c.Logger().Error(err)
			s.errorHandler(c, err)
		}
	}
}
