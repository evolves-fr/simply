package simply

type Store interface {
	Get(key string) any
	Set(key string, value any)
}

func NewStore() Store {
	return make(store)
}

type store map[string]any

func (s store) Get(key string) any {
	return s[key]
}

func (s store) Set(key string, value any) {
	s[key] = value
}
