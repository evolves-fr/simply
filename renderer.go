package simply

import (
	"bytes"
	"html/template"
	"io"
	"io/fs"
	"io/ioutil"
	"net/http"
	"path/filepath"

	"github.com/Masterminds/sprig/v3"
	"github.com/gorilla/mux"
)

type Renderer interface {
	Render(io.Writer, string, any, Context) error
	RenderWithLayout(io.Writer, string, string, any, Context) error
	Error(w io.Writer, e error, c Context) error
}

func NewRenderer(fs fs.FS) Renderer {
	return &renderer{
		fs:          fs,
		baseFolder:  "web",
		functions:   sprig.FuncMap(),
		errorView:   "error.gohtml",
		errorLayout: "base.gohtml",
	}
}

type renderer struct {
	fs          fs.FS
	baseFolder  string
	functions   template.FuncMap
	errorView   string
	errorLayout string
}

func (r *renderer) Render(w io.Writer, view string, i any, c Context) error {
	// Open view file
	file, err := r.fs.Open(filepath.Join(r.baseFolder, "/views/", view))
	if err != nil {
		return err
	}

	// Read view file
	content, err := ioutil.ReadAll(file)
	if err != nil {
		return err
	}

	// Extends template function
	functions := r.functions
	functions["Store"] = func() Store { return c.Store() }
	functions["Request"] = func() *http.Request { return c.Request() }
	functions["Params"] = func() Params { return params(mux.Vars(c.Request())) }

	// Parse view template
	tpl, err := template.New(filepath.Base(view)).Funcs(functions).Parse(string(content))
	if err != nil {
		return err
	}

	// Execute view template with data to writer
	return tpl.Execute(w, i)
}

func (r *renderer) RenderWithLayout(w io.Writer, layout, view string, i any, c Context) error {
	// Execute view template
	var viewContent = new(bytes.Buffer)
	if err := r.Render(viewContent, view, i, c); err != nil {
		return err
	}

	// Open layout file
	file, err := r.fs.Open(filepath.Join(r.baseFolder, "/layouts/", layout))
	if err != nil {
		return err
	}

	// Read layout file
	content, err := ioutil.ReadAll(file)
	if err != nil {
		return err
	}

	// Extends template function
	functions := r.functions
	functions["Store"] = func() Store { return c.Store() }
	functions["Request"] = func() *http.Request { return c.Request() }
	functions["Params"] = func() Params { return params(mux.Vars(c.Request())) }
	functions["Content"] = func() template.HTML { return template.HTML(viewContent.String()) }

	// Parse layout template
	tpl, err := template.New(filepath.Base(view)).Funcs(functions).Parse(string(content))
	if err != nil {
		return err
	}

	// Execute layout template with data to writer
	return tpl.Execute(w, i)
}

func (r *renderer) Error(w io.Writer, e error, c Context) error {
	return r.RenderWithLayout(w, r.errorLayout, r.errorView, e, c)
}
