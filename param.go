package simply

type Params interface {
	Get(key string) string
}

type params map[string]string

func (p params) Get(key string) string {
	return p[key]
}
