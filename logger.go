package simply

import (
	"github.com/sirupsen/logrus"
	"io"
)

type Logger interface {
	Print(...any)
	Printf(string, ...any)
	Debug(args ...any)
	Debugf(string, ...any)
	Info(args ...any)
	Infof(string, ...any)
	Warning(args ...any)
	Warningf(string, ...any)
	Error(args ...any)
	Errorf(string, ...any)
	Fatal(...any)
	Fatalf(string, ...any)
	Panic(...any)
	Panicf(string, ...any)
}

func NewLogger(lvl Level, out io.Writer) Logger {
	logger := logrus.New()

	logger.SetLevel(logrus.Level(lvl))
	logger.SetOutput(out)
	logger.SetFormatter(&logrus.TextFormatter{ForceQuote: true, FullTimestamp: true})

	return logger
}

const (
	LevelPanic Level = iota
	LevelFatal
	LevelError
	LevelWarning
	LevelInfo
	LevelDebug
	LevelTrace
)

type Level uint64
